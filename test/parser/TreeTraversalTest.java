package parser;

import org.junit.Test;
import static org.junit.Assert.*;

public class TreeTraversalTest {
    // 1 + 2
    ExpressionTree expr1 = new ExpressionTree("+",          //  +
                               new ExpressionTree("1"),     // 1 2
                               new ExpressionTree("2"));
    
    // 1 + 2 * 3
    ExpressionTree expr2 = new ExpressionTree("+",          //  +
                               new ExpressionTree("1"),     // 1  *
                               new ExpressionTree("*",      //   2 3
                                   new ExpressionTree("2"),
                                   new ExpressionTree("3")));
    // (1 + 2) * 3
    ExpressionTree expr3 = new ExpressionTree("*",          //    *
                               new ExpressionTree("+",      //  +  3
                                   new ExpressionTree("1"), // 1 2
                                   new ExpressionTree("2")),
                               new ExpressionTree("3"));
    
    private String postfix(ExpressionTree expr) {
        if (expr.isLeaf())
            return expr.getExpression();
        
        return postfix(expr.getLeft()) + " " + postfix(expr.getRight()) + " " + expr.getExpression();
    }
    
    private String infix(ExpressionTree expr) {
        if (expr.isLeaf())
            return expr.getExpression();
        
        return  "(" + infix(expr.getLeft()) + " " + expr.getExpression() + " " + infix(expr.getRight()) + ")";
    }
    
    @Test
    public void postfixExpression1() {
        assertEquals("1 2 +", postfix(expr1));
    }
    
    @Test
    public void postfixExpression2() {
        assertEquals("1 2 3 * +", postfix(expr2));
    }
    
    @Test
    public void postfixExpression3() {
        assertEquals("1 2 + 3 *", postfix(expr3));
    }
    
    @Test
    public void infixExpression1() {
        assertEquals("(1 + 2)", infix(expr1));
    }
    
    @Test
    public void infixExpression2() {
        assertEquals("(1 + (2 * 3))", infix(expr2));
    }
    
    @Test
    public void infixExpression3() {
        assertEquals("((1 + 2) * 3)", infix(expr3));
    }
}
