package parser;

import org.junit.Test;
import static org.junit.Assert.*;

public class BracketsValidatorTest {
    
    private void assertResult(String expression, String expectedResult) {
        BracketsValidator validator = new StackBracketsValidator();
        
        assertEquals(expectedResult, validator.validate(expression));
    }
    
    @Test
    public void emptyStringIsValid() {
        assertResult("", "ok");
    }
    
    @Test
    public void oneRightRoundBracketIsInvalid() {
        assertResult(")", "^ unexpected )");
    }
    
    @Test
    public void leftRightRoundBracketsAreValid() {
        assertResult("()", "ok");
    }
    
    @Test
    public void twoLeftRightRoundBracketsAreValid() {
        assertResult("()()", "ok");
    }
    
    @Test
    public void nestedLeftRightRoundBracketsAreValid() {
        assertResult("(())", "ok");
    }
    
    @Test
    public void oneRightSquareBracketIsInvalid() {
        assertResult("]", "^ unexpected ]");
    }
    
    @Test
    public void leftRightSquareBracketsAreValid() {
        assertResult("[]", "ok");
    }
    
    @Test
    public void leftRoundRightSquareBracketsAreInvalid() {
        assertResult("(]", " ^ expected )");
    }
    
    @Test
    public void extraRightSquareBracketsIsInvalid() {
        assertResult("[]]", "  ^ unexpected ]");
    }
    
    @Test
    public void oneLeftSquareBracketIsInvalid() {
        assertResult("[", " ^ expected ]");
    }
}
