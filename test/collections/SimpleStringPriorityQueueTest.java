package collections;

import org.junit.Test;
import static org.junit.Assert.*;

public class SimpleStringPriorityQueueTest {
    @Test
    public void newQueueHasSizeZero() {
        SimpleStringPriorityQueue queue = new SimpleStringPriorityQueue();
        
        assertEquals(0, queue.size());
    }
    
    @Test
    public void queueHasSizeOneAfterEnqueue() {
        SimpleStringPriorityQueue queue = new SimpleStringPriorityQueue();
        
        queue.enqueue("test", 1);
        
        assertEquals(1, queue.size());
    }
    
    @Test
    public void queueHasSizeZeroAfterDequeue() {
        SimpleStringPriorityQueue queue = new SimpleStringPriorityQueue();
        
        queue.enqueue("test", 1);
        queue.dequeue();
        
        assertEquals(0, queue.size());
    }
    
    @Test
    public void elementIsDequeued() {
        SimpleStringPriorityQueue queue = new SimpleStringPriorityQueue();
        
        queue.enqueue("test", 1);
        
        String element = queue.dequeue();
        
        assertEquals("test", element);
    }
    
    @Test
    public void elementsAreDequeuedInFifoOrder() {
        SimpleStringPriorityQueue queue = new SimpleStringPriorityQueue();
        
        queue.enqueue("test", 2);
        queue.enqueue("test2", 2);
        
        assertEquals("test", queue.dequeue());
        assertEquals("test2", queue.dequeue());
    }
    
    @Test
    public void elementsAreDequeuedInPriorityOrder() {
        SimpleStringPriorityQueue queue = new SimpleStringPriorityQueue();
        
        queue.enqueue("test", 3);
        queue.enqueue("test2", 2);
        
        assertEquals("test2", queue.dequeue());
        assertEquals("test", queue.dequeue());
    }
    
    @Test
    public void elementsAreDequeuedInPriorityFifoOrder() {
        SimpleStringPriorityQueue queue = new SimpleStringPriorityQueue();
        
        queue.enqueue("test1", 2);
        queue.enqueue("test2", 3);
        queue.enqueue("test3", 2);
        
        assertEquals("test1", queue.dequeue());
        assertEquals("test3", queue.dequeue());
        assertEquals("test2", queue.dequeue());
    }
    
    @Test
    public void dequeueReturnsNullIfQueueIsEmpty() {
        SimpleStringPriorityQueue queue = new SimpleStringPriorityQueue();
        
        String element = queue.dequeue();
        
        assertNull(element);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void enqueueNullThrowsException() {
        SimpleStringPriorityQueue queue = new SimpleStringPriorityQueue();
        
        queue.enqueue(null, 0);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void enqueueNegativePriorityThrowsException() {
        SimpleStringPriorityQueue queue = new SimpleStringPriorityQueue();
        
        queue.enqueue("test", -1);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void enqueueTooLargePriorityThrowsException() {
        SimpleStringPriorityQueue queue = new SimpleStringPriorityQueue();
        
        queue.enqueue("test", 10);
    }
}
