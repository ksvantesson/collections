package collections;

import org.junit.Test;
import static org.junit.Assert.*;

public class SimpleStringListTest {
    @Test
    public void newListHasSizeZero() {
        SimpleStringList list = new SimpleStringList();
        
        assertEquals(0, list.size());
    }
    
    @Test
    public void listHasSizeOneAfterAdd() {
        SimpleStringList list = new SimpleStringList();
        
        list.add(0, "test");
        
        assertEquals(1, list.size());
    }
    
    @Test
    public void listContainsElementAfterAdd() {
        SimpleStringList list = new SimpleStringList();
        
        list.add(0, "test");
        
        assertEquals("test", list.get(0));
    }
    
    @Test
    public void listHasSizeZeroAfterRemove() {
        SimpleStringList list = new SimpleStringList();
        
        list.add(0, "test");
        list.remove(0);
        
        assertEquals(0, list.size());
    }
    
    @Test(expected = IndexOutOfBoundsException.class)
    public void removeThrowsExceptionIfInvalidIndex() {
        SimpleStringList list = new SimpleStringList();
        
        list.remove(0);
    }
    
    @Test(expected = IndexOutOfBoundsException.class)
    public void addThrowsExceptionIfInvalidIndex() {
        SimpleStringList list = new SimpleStringList();
        
        list.add(1, "test");
    }
    
    @Test(expected = IndexOutOfBoundsException.class)
    public void addThrowsExceptionIfNegativeIndex() {
        SimpleStringList list = new SimpleStringList();
        
        list.add(-1, "test");
    }
    
    @Test(expected = IndexOutOfBoundsException.class)
    public void getThrowsExceptionIfInvalidIndex() {
        SimpleStringList list = new SimpleStringList();
        
        list.get(0);
    }
}
