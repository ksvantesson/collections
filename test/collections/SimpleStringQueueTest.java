package collections;

import org.junit.Test;
import static org.junit.Assert.*;

public class SimpleStringQueueTest {
    @Test
    public void newQueueHasSizeZero() {
        SimpleStringQueue queue = new SimpleStringQueue();
        
        assertEquals(0, queue.size());
    }
    
    @Test
    public void queueHasSizeOneAfterEnqueue() {
        SimpleStringQueue queue = new SimpleStringQueue();
        
        queue.enqueue("test");
        
        assertEquals(1, queue.size());
    }
    
    @Test
    public void queueHasSizeZeroAfterDequeue() {
        SimpleStringQueue queue = new SimpleStringQueue();
        
        queue.enqueue("test");
        queue.dequeue();
        
        assertEquals(0, queue.size());
    }
    
    @Test
    public void elementIsDequeued() {
        SimpleStringQueue queue = new SimpleStringQueue();
        
        queue.enqueue("test");
        
        String element = queue.dequeue();
        
        assertEquals("test", element);
    }
    
    @Test
    public void elementsAreDequeuedInFifoOrder() {
        SimpleStringQueue queue = new SimpleStringQueue();
        
        queue.enqueue("test");
        queue.enqueue("test2");
        
        assertEquals("test", queue.dequeue());
        assertEquals("test2", queue.dequeue());
    }
    
    @Test
    public void dequeueReturnsNullIfQueueIsEmpty() {
        SimpleStringQueue queue = new SimpleStringQueue();
        
        String element = queue.dequeue();
        
        assertNull(element);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void enqueueNullThrowsException() {
        SimpleStringQueue queue = new SimpleStringQueue();
        
        queue.enqueue(null);
    }
}
