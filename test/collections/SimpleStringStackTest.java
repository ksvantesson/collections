package collections;

import java.util.EmptyStackException;
import org.junit.Test;
import static org.junit.Assert.*;

public class SimpleStringStackTest {
    private StringStack CreateStack() {
        return new SimpleStringStack();
    }
    
    @Test
    public void newStackIsEmpty() {
        StringStack stack = CreateStack();
        
        assertTrue(stack.empty());
    }
    
    @Test
    public void stackIsNotEmptyAfterPush() {
        StringStack stack = CreateStack();
        
        stack.push("test");
        
        assertFalse(stack.empty());
    }
    
    @Test
    public void stackIsEmptyAfterPushAndPop() {
        StringStack stack = CreateStack();
        
        stack.push("test");
        stack.pop();
        
        assertTrue(stack.empty());
    }
    
    @Test
    public void stackIsNotEmptyAfterPushAndPeek() {
        StringStack stack = CreateStack();
        
        stack.push("test");
        stack.peek();
        
        assertFalse(stack.empty());
    }
    
    @Test
    public void popRemovesPushedElement() {
        StringStack stack = CreateStack();
        
        stack.push("test");
        
        String element = stack.pop();
        
        assertEquals("test", element);
    }
    
    @Test
    public void popRemovesTopElement() {
        StringStack stack = CreateStack();
        
        stack.push("test");
        stack.push("test2");
        
        String element = stack.pop();
        
        assertEquals("test2", element);
    }
    
    @Test(expected = EmptyStackException.class)
    public void popThrowsExceptionIfStackIsEmpty() {
        StringStack stack = CreateStack();
        
        stack.pop();
    }
    
    @Test
    public void peekReturnsPushedElement() {
        StringStack stack = CreateStack();
        
        stack.push("test");
        
        String element = stack.peek();
        
        assertEquals("test", element);
    }
    
    @Test
    public void peekReturnsTopElement() {
        StringStack stack = CreateStack();
        
        stack.push("test");
        stack.push("test2");
        
        String element = stack.peek();
        
        assertEquals("test2", element);
    }
    
    @Test(expected = EmptyStackException.class)
    public void peekThrowsExceptionIfStackIsEmpty() {
        StringStack stack = CreateStack();
        
        stack.peek();
    }
}
