package parser;

public class ExpressionTree {
    private String expression;
    private ExpressionTree left;
    private ExpressionTree right;
    
    public ExpressionTree(String expression) {
        this.expression = expression;
    }
    
    public ExpressionTree(String expression, ExpressionTree left, ExpressionTree right) {
        this.expression = expression;
        this.left = left;
        this.right = right;
    }
    
    public String getExpression() {
        return expression;
    }
    
    public ExpressionTree getLeft() {
        return left;
    }

    public ExpressionTree getRight() {
        return right;
    }
    
    public boolean isLeaf() {
        return left == null && right == null;
    }
}
