package parser;

import collections.SimpleStringStack;
import java.util.HashMap;

public class StackBracketsValidator implements BracketsValidator {
    static private HashMap<String, String> brackets;
    
    static {
        brackets = new HashMap<>();
        brackets.put("(", ")");
        brackets.put("[", "]");
    }
    
    @Override
    public String validate(String expression) {
        SimpleStringStack stack = new SimpleStringStack();
        
        for (int i = 0; i < expression.length(); i++) {
            String bracket = expression.substring(i, i + 1);
            
            if (brackets.containsKey(bracket)) {
                stack.push(brackets.get(bracket));
            } else if (stack.empty()) {
                return formatUnexpected(i, bracket);
            } else {
                String expected = stack.pop();

                if (!expected.equals(bracket))
                    return formatExpected(i, expected);
            }
        }
        
        if (!stack.empty())
            return formatExpected(expression.length(), stack.pop());

        return "ok";
    }

    private String formatUnexpected(int padding, String unexpected) {
        return createPadding(padding) + "^ unexpected " + unexpected;
    }

    private String formatExpected(int padding, String expected) {
        return createPadding(padding) + "^ expected " + expected;
    }

    private String createPadding(int count) {
        String padding = "";
        
        for (int i = 0; i < count; i++)
            padding += " ";
        
        return padding;
    }
    
}
