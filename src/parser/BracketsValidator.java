package parser;

public interface BracketsValidator {
    public String validate(String expression);
}
