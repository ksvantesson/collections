package collections;

import java.util.ArrayList;

public class SimpleStringList {
    private ArrayList<String> list = new ArrayList<>();
    
    public void add(int index, String element) {
        list.add(index, element);
    }

    public String get(int index) {
        return list.get(index);
    }

    public void remove(int index) {
        list.remove(index);
    }
    
    public int size() {
        return list.size();
    }
}
