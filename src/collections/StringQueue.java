package collections;

public interface StringQueue {
    public String dequeue();
    public void enqueue(String element);
    public int size();
}
