package collections;

public interface StringStack {
    public boolean empty();
    public String peek();
    public String pop();
    public void push(String element);
}
