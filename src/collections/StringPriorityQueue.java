package collections;

public interface StringPriorityQueue {
    public String dequeue();
    public void enqueue(String element, int priority);
    public int size();
}
