package collections;

public class SimpleStringQueue implements StringQueue {
    private SimpleStringList list = new SimpleStringList();

    @Override
    public String dequeue() {
        if (size() == 0)
            return null;
        
        int lastIndex = size() - 1;
        String element = list.get(lastIndex);
        
        list.remove(lastIndex);
        
        return element;
    }

    @Override
    public void enqueue(String element) {
        if (element == null)
            throw new IllegalArgumentException();
        
        list.add(0, element);
    }

    @Override
    public int size() {
        return list.size();
    }
}
