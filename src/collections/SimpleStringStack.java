package collections;

import java.util.EmptyStackException;

public class SimpleStringStack implements StringStack {
    private SimpleStringList list = new SimpleStringList();

    @Override
    public boolean empty() {
        return list.size() == 0;
    }

    @Override
    public String peek() {
        if (empty())
            throw new EmptyStackException();
        
        return list.get(0);
    }

    @Override
    public String pop() {
        String element = peek();
        
        list.remove(0);
        
        return element;
    }

    @Override
    public void push(String element) {
        list.add(0, element);
    }
}
