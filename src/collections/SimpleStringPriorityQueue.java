package collections;

import java.util.ArrayList;

public class SimpleStringPriorityQueue implements StringPriorityQueue {
    private ArrayList<Element> list = new ArrayList<>();
    
    @Override
    public String dequeue() {
        if (size() == 0)
            return null;
        
        int lastIndex = size() - 1;
        String element = list.get(lastIndex).value;
        
        list.remove(lastIndex);
        
        return element;
    }

    @Override
    public void enqueue(String element, int priority) {
        if (element == null || priority < 0 || priority > 9)
            throw new IllegalArgumentException();
        
        int index = getIndexFor(priority);
        
        list.add(index, new Element(priority, element));
    }

    private int getIndexFor(int priority) {
        int index = 0;
        
        for (Element element : list) {
            if (element.priority <= priority)
                break;
            
            index++;
        }
        
        return index;
    }

    @Override
    public int size() {
        return list.size();
    }
    
    private class Element {
        public int priority;
        public String value;
        
        public Element(int priority, String value) {
            this.priority = priority;
            this.value = value;
        }
    }
}
